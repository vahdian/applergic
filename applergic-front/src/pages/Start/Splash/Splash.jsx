import React from 'react'
import { Link } from 'react-router-dom';
import './splash.scss'

export default function Splash() {
    return (
        
        <div>
        <Link to = { '/on-boarding/' }>
        <div className = "splash" >
            
            <h1>Applergic</h1>
            <h3>Mi guia alimentaria</h3>
        </div>
        </Link>
        </div>
    )
}
