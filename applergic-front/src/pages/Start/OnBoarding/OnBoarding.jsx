import React, { useEffect, useState } from 'react'
import logo from '../../../assets/img/Start/logo.png'
import rectangle from '../../../assets/img/Start/rectangle.png'
import scan from '../../../assets/img/Start/scan2.png'
import ambulancia from '../../../assets/img/Start/ambulancia.png'
import traduccion from '../../../assets/img/Start/traduccioN.png'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, EffectFlip } from 'swiper';
import { Link } from 'react-router-dom';
import 'swiper/components/effect-fade/effect-fade.scss';

SwiperCore.use([Navigation, Pagination]);
SwiperCore.use([EffectFlip]);

export default function OnBoarding(){
    const slides = [
        {photo : scan, text: "¡Bienvenido a Applergic! Escanea el código de barras de tu producto y Applergic te dirá si es apto para ti" }, 
        {photo: rectangle, text:"Lleva tu Diario de compras y actividades" }, 
        {photo : ambulancia, text: "En caso de emergencia nos pondremos en contacto con la persona que nos digas."},
        {photo : traduccion, text : "Viaja a donde quieras. Tendrás a tu disposición un traductor off-line y tu informe de alergias e intolerancias traducidos al idioma local"}
        ]

    const [indexState, setIndex]= useState(0);
    
    const next = (next, i) => {
        i = indexState + 1;
        console.log('datanext ' + next)
        console.log ('fn '+ i)

        
        
    }
    console.log('indexState ' + indexState)
 
    // useEffect(next, [])
    return (
        <div>
            <img src={logo} alt="logo Applergic"></img>
        
        <Swiper effect="flip"
    
            spaceBetween={50}
            slidesPerView={1}
            pagination={{ clickable: true }}
            onSlideChange={($event) => {

                
                // next($event.activeIndex); 
    
                console.log('onsilde' +$event.activeIndex);
                // console.log('onsilde' +$eve);
                
                next()
                   return setIndex($event.activeIndex, indexState); 
                
            } 
            //     //crear variable de estado con el valor de $event.activeIndex
            //      
            }
            onSwiper={(swiper) => console.log(swiper)}

        >

        {slides.map((slide, i) => {
        return <SwiperSlide key={i} className="mb-3"><img src={slide.photo}></img><p>{slide.text}</p></SwiperSlide>;
        })}

        <div className="swiper-pagination "></div>
            
        <div>
            <Link to = { '/login' }><button>Saltar</button></Link>
            {slides && slides.length ? <button key={indexState} onClick={() => next(indexState)}>Siguiente</button> :
            <button key={indexState} onClick={next(setIndex, indexState)}>Terminar</button> } 
            </div>
        </Swiper>

        
        
        </div>
    )
}
