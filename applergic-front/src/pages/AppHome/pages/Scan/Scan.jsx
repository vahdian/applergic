import React from 'react'
import { Link } from 'react-router-dom';
import './Scan.scss';
// import Button from '../../../../core/components/Button/Button'
import ScanCode from './ScanCode/ScanCode';

export default function Scan() {
    
    return (
        <div>
           <Link to = { '/home/' }><button>X</button></Link>
            <h3>Escaneando...</h3>
            <p>Tan solo tienes que centrar el <span>código de barras</span> del producto en el recuadro</p>
            <ScanCode></ScanCode>
            <div className="cont">
            <div className="block">
            <button className="button button__barCode button__active"></button><p>Codigo de barras</p></div>
            <Link to = { '/scanQr' } ><button className="button button__qr"></button><p>Codigo Qr</p></Link>
            </div>
        </div>
    )
}
