import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import '../Scan.scss';
import Test from './QrClass';

export default function Scan() {
  
    return (
        <div>
           <Link to = { '/home/' }><button>X</button></Link>
            <h3>Escaneando...</h3>
            <p>Tan solo tienes que centrar el <span>código de barras</span> del producto en el recuadro</p>
            
            <Test></Test>
            
            <div>
            <Link to = { '/scan' } ><button className="button button__barCode"></button><p>Codigo de barras</p></Link>
            <button className="button button__qr button__active"></button><p>Codigo Qr</p>
            </div>
            
        </div>
    )
}