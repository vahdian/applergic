import React, { Component } from 'react'
import QrReader from 'react-qr-reader'

// npm i react-qr-reader este es el bueno

class Test extends Component {
  state = {
    result: 'No result'
  }
 
  handleScan = data => {
    if (data) {
      this.setState({
        result: data,
        console : console.log(data)
      })
    }
  }
  handleError = err => {
    console.error(err)
  }
  render() {
    return (
      <div>
        <QrReader
          delay={300}
          onError={this.handleError}
          onScan={this.handleScan}
          style={{ maxwidth: '100%', border: '2px solid black'}}
        />
        
        <p>{this.state.result}</p>
        
      </div>
    )
  }
}
export default Test;
