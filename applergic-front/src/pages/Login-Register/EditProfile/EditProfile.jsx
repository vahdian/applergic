import React from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import camera from '../../../assets/img/Login-Register/cMaraAzul.png';
import './editProfile.scss';

export default function EditProfile() {

    const { register, handleSubmit, errors, reset } = useForm();

    function doSubmit (data) {
        console.log(data);
        console.log("FORM OK");
        reset();
    }

    function selectPhoto () {
        console.log("subir foto");
    }

    return (
        <div>

            <div>
                <span><Link to="/login"> {'<'} Volver</Link></span>
                <p>1 de 4</p>
            </div>

            <div>
                <h2>Dinos quien eres.</h2>

                <div className="photo">
                    <figure onClick={selectPhoto()}>
                        <img src={camera} alt="camera" ></img>
                        <figcaption className="photo__text">Subir foto</figcaption>
                    </figure>
                </div>

            </div>

            <div>

                <form onSubmit={handleSubmit(doSubmit)}>

                    <div>
                        <label htmlFor="name">
                            <input type="text" name="name" placeholder="Nombre completo" ref={register({required: true})}/>
                        </label>
                        {errors.name && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <label htmlFor="email">
                            <input type="email" name="email" placeholder="Dirección email" ref={register({required: true})}/>
                        </label>
                        {errors.email && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <label htmlFor="mobile">
                            <input type="text" name="mobile" placeholder="Móvil" ref={register({required: true})}/>
                        </label>
                        {errors.mobile && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <label htmlFor="password">
                            <input type="password" name="password" placeholder="Contraseña" ref={register({required: true})}/>
                        </label>
                        {errors.password && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <input type="submit" value="Guardar perfil" />                    
                    </div>

                </form>

            </div>

            <div>
                <button><Link to="/edit-ct">SIGUIENTE PRUEBA</Link></button>
            </div>

        </div>
    )
}
