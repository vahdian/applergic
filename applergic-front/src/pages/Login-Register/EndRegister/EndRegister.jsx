import React from 'react';
import { Link } from 'react-router-dom';
import ok from '../../../assets/img/Login-Register/ok.png';

export default function EndRegister() {
    return (
        <div>

            <div>
                <span><Link to="/confirm-allergy"> {'<'} Volver</Link></span>
                <p>4 de 4</p>
            </div>

            <div>
                <figure>
                    <img src={ok} alt="okey"></img>
                    <figcaption>Hemos terminado, ya puedes escanear tu primer producto.</figcaption>
                </figure>
            </div>

            <div>
                <button><Link to="/scan">Escanea un producto</Link></button>
            </div>
            
        </div>
    )
}
