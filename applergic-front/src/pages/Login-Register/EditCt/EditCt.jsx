import React from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";

export default function EditCt() {

    const { register, handleSubmit, errors, reset } = useForm();

    function doSubmit (data) {
        console.log(data);
        console.log("FORM OK");
        reset();
    }

    return (
        <div>

            <div>
                <span><Link to="/edit-profile"> {'<'} Volver</Link></span>
                <p>2 de 4</p>
            </div>

            <div>
                <h2>Vamos a añadir tu contacto en caso de emergencia.</h2>
                <p>Nos pondremos en contacto con tu persona de confianza y/o compañia de seguros en caso de emergencia.</p>
            </div>

            <div>

                <form onSubmit={handleSubmit(doSubmit)}>

                    <div>
                        <label htmlFor="name">
                            <input type="text" name="name" placeholder="Nombre completo de tu contacto" ref={register({required: true})}/>
                        </label>
                        {errors.name && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <label htmlFor="email">
                            <input type="email" name="email" placeholder="Dirección email" ref={register({required: true})}/>
                        </label>
                        {errors.email && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <label htmlFor="mobile">
                            <input type="text" name="mobile" placeholder="Móvil" ref={register({required: true})}/>
                        </label>
                        {errors.mobile && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <label htmlFor="insurance">
                            <input type="text" name="insurance" placeholder="Compañia de Seguros / Nº Póliza" ref={register({required: true})}/>
                        </label>
                        {errors.insurance && <span>Campo obligatorio</span> }
                    </div>

                    <div>
                        <input type="submit" value="Guardar emergencias" />                    
                    </div>

                </form>

                <span> <Link to="/login">Me registraré en otro momento</Link> </span>

            </div>

            <div>
                <button><Link to="/config-allergy">SIGUIENTE PRUEBA</Link></button>
            </div>

        </div>
    )
}