import React from 'react';
import { Link } from 'react-router-dom';

export default function ConfirmAllergy() {
    return (
        <div>

            <div>
                <h2>Confirma tu selección.</h2>
                <p>A continuación te resumimos los alimentos registrados como peligrosos para ti.</p>
            </div>

            <div>
                Seleccion alergenos
                <button><Link to="/config-allergy">Añadir nuevos</Link></button>
            </div>

            <div>
                <button><Link to="/end-register">Confirmar</Link></button>
            </div>
            
        </div>
    )
}
