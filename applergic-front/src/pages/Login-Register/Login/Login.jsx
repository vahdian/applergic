import React from 'react';
import food from '../../../assets/img/Login-Register/image.png';
import logo from '../../../assets/img/Start/logo.png';
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import './Login.scss';

export default function Login() {

    const { register, handleSubmit, errors, reset } = useForm();

    function doSubmit (data) {
        console.log(data);
        console.log("FORM OK");
        reset();
    }

    return (
        <div className="login">

            <div className="login__intro">

                <div className="login__intro__img">
                    <img src={food}></img>
                    <img className="login__logo" src={logo}></img>
                </div>
                
                <div className="login__intro__text">
                    <h1>¡Bienvenido de nuevo!</h1>
                    <p>Por favor, introduce tus datos para continuar.</p>
                </div>
                
            </div>

            <div className="login__form">

                <form onSubmit={handleSubmit(doSubmit)}>

                    <div className="login__form__input">
                        <label htmlFor="email">
                            <input type="email" name="email" placeholder="Dirección email" ref={register({required: true})}/>
                        </label>
                        {errors.email && <span>El campo email es obligatorio</span> }
                    </div>

                    <div className="login__form__input">
                        <label htmlFor="password">
                            <input type="password" name="password" placeholder="Contraseña" ref={register({required: true})} />
                        </label>
                        {errors.password && <span>El campo contraseña es obligatorio</span> }
                        <Link to="/">¿Olvidaste tu contraseña?</Link> 
                    </div>

                    <div className="login__form__input--submit">
                        <input type="submit" value="Entrar" />                    
                    </div>

                </form>

            </div>

            <div className="login__new">
                <span>¿nuevo en Applergic?</span>
                <span> <Link to="/edit-profile">Crea tu cuenta aquí</Link> </span>
                <span> <Link to="/config-allergy">Me registraré en otro momento</Link> </span>
            </div>

        </div>
    )
}
