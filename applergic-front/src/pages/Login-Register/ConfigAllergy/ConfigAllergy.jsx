import React from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";

export default function ConfigAllergy() {

    const { register, handleSubmit, reset } = useForm();

    function doSubmit (data) {
        console.log(data);
        console.log("FORM OK");
        reset();
    }

    return (
        <div>

            <div>
                <span><Link to="/edit-ct"> {'<'} Volver</Link></span>
                <p>3 de 4</p>
            </div>

            <div>
                <h2>Ahora selecciona tu alergias e intolerancias.</h2>
                <p>Los elementos marcados serán identificados en tus busquedas como peligrosos para ti.</p>
            </div>

            <div>

                <button>A</button>
                <button>C</button>
                <button>F</button>
                <button>G</button>
                <button>H</button>
                <button>K</button>
                <button>L</button>
                <button>M</button>
                <button>N</button>
                <button>P</button>
                <button>R</button>
                <button>S</button>
                <button>T</button>
                <button>U</button>
                <button>V</button>
                <button>Y</button>

            </div>

            <div>

                <form onSubmit={handleSubmit(doSubmit)}>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="A">
                                <option value="acid">Ácido benzoico</option>
                                <option value="almond">Almendras</option>
                                <option value="lupine">Altramuces</option>
                                <option value="cashew">Anacardos</option>
                                <option value="celery">Apio</option>
                                <option value="rice">Arroz</option>
                                <option value="hazelnut">Avellana</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="C">
                                <option value="peanut">Cacahuete</option>
                                <option value="cocoa">Cacao</option>
                                <option value="chestnut">Castaña</option>
                                <option value="cereals">Cereales</option>
                                <option value="coconut">Coco</option>
                                <option value="crustaceans">Crustáceos</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="F">
                                <option value="phenylalanine">Fenilalanina</option>
                                <option value="fibers">Fibras</option>
                                <option value="strawberry">Fresa</option>
                                <option value="fructose">Fructosa</option>
                                <option value="fruits">Frutas</option>
                                <option value="nuts">Frutos con cáscara</option>
                                <option value="berries">Frutos rojos</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="G">
                                <option value="jelly">Gelatina</option>
                                <option value="pea">Guisante</option>
                                <option value="glucose">Glucosa</option>
                                <option value="gluten">Gluten</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="H">
                                <option value="egg">Huevo</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="K">
                                <option value="kiwi">Kiwi</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="L">
                                <option value="lactose">Lactosa</option>
                                <option value="milk">Leche</option>
                                <option value="legume">Legumbres</option>
                                <option value="lentil">Lenteja</option>
                                <option value="linen">Lino</option>
                                <option value="ltp">LTP</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="M">
                                <option value="corn">Maiz</option>
                                <option value="seafood">Marisco</option>
                                <option value="peach">Melocotón</option>
                                <option value="mollusk">Moluscos</option>
                                <option value="mustard">Mostaza</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="N">
                                <option value="walnut">Nueces</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="P">
                                <option value="fish">Pescado</option>
                                <option value="pinion">Piñones</option>
                                <option value="pistachio">Pistachos</option>
                                <option value="banana">Plátano</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="R">
                                <option value="rosaceae">Rosáceas</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="S">
                                <option value="sesame">Sésamo</option>
                                <option value="soy">Soja</option>
                                <option value="sorbitol">Sorbitol</option>
                                <option value="sulphites">Sulfitos</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="T">
                                <option value="tomato">Tomate</option>
                                <option value="traces">Trazas</option>
                                <option value="wheat">Trigo</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="U">
                                <option value="grape">Uva</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="V">
                                <option value="vitaminD">Vitamina D</option>
                                <option value="vitaminE">Vitamina E</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <label htmlFor="allergyChoice">
                            <select name="Y">
                                <option value="yucca">Yuca</option>
                            </select>
                        </label>
                    </div>

                    <div>
                        <input type="submit" value="Guardar" />                    
                    </div>

                </form>

            </div>

            <div>
                <button><Link to="/confirm-allergy">SIGUIENTE PRUEBA</Link></button>
            </div>

        </div>
    )
}